import 'package:alura_bytebank/screens/contacts_list.dart';
import 'package:alura_bytebank/screens/dashboard.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const Bytebank());
}

class Bytebank extends StatelessWidget {
  const Bytebank({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ContactsList(),
      debugShowCheckedModeBanner: false,
    );
  }
}
